/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.echocat.kata.java.domain.Book;
import org.echocat.kata.java.domain.Item;
import org.echocat.kata.java.domain.Magazine;
import org.echocat.kata.java.dto.Library;
import org.echocat.kata.java.utils.CsvWriter;
import org.echocat.kata.java.utils.LibraryCommand;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class to handle command operations and execute them using:
 *
 * <ul>
 *  <li> Library Service
 *  <li> Search Service
 * </ul>
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class CommandService {

    /**
     * Define logger to track errors and show messages.
     */
    private static final Logger LOG = LogManager.getLogger(CommandService.class);

    /**
     * Define line separator for test cases.
     */
    private static final String LINE_SEPARATOR = System.lineSeparator();

    /**
     * Define separator of arguments for commands.
     */
    private static final String SEPARATOR = " ";

    /**
     * Define base error for the command service.
     */
    private static final String BASE_ERROR = "Command not supported";

    /**
     * Define no found string result.
     */
    private static final String NOT_FOUND = "NOT FOUND";

    /**
     * Define added string result.
     */
    private static final String ADDED = "ADDED";

    /**
     * Define search service to perform queries over library.
     */
    private final SearchService searchService;

    /**
     * Define instance to write csv files.
     */
    private final CsvWriter csvWriter;

    /**
     * Constructor of command service to initialize services.
     *
     * @param searchService Search service used to perform operations against a library.
     * @param csvWriter     Csv writer instance used to export library files.
     */
    public CommandService(final SearchService searchService, final CsvWriter csvWriter) {
        this.searchService = searchService;
        this.csvWriter = csvWriter;
    }

    /**
     * Receives the input of the console and execute the respective command.
     *
     * @param input Input in command line.
     * @return Result of the command operation to print it in console.
     */
    public String execute(final String input) {

        String[] execution = getExecution(input);

        final LibraryCommand libraryCommand = LibraryCommand.findByCommandAndArgsLength(execution[0], execution.length - 1);

        if (LibraryCommand.PRINT.equals(libraryCommand))
            return printLibrary();

        if (LibraryCommand.FIND_BY_ISBN.equals(libraryCommand))
            return findByIsbn(execution[1]);

        if (LibraryCommand.FIND_BY_AUTHORS.equals(libraryCommand))
            return findByAuthors(execution[1]);

        if (LibraryCommand.SORT.equals(libraryCommand))
            return sortByTitle();

        if (LibraryCommand.ADD.equals(libraryCommand))
            return addItem(execution[1], execution[2]);

        return BASE_ERROR;
    }

    /**
     * Process the input to transform into execution command and parameters for the supported library commands.
     *
     * @param input Input in command line.
     * @return Array of strings with execution command and parameters.
     */
    private String[] getExecution(final String input) {

        String[] execution = (null == input) ? new String[1] : input.trim().split(SEPARATOR);

        if (null != input && LibraryCommand.ADD.getCommand().equals(execution[0])) {

            final String[] adjustedExecution = new String[3];

            adjustedExecution[0] = execution[0];
            adjustedExecution[1] = execution[1];

            final int startIndex = input.indexOf(execution[1]) + execution[1].length();
            adjustedExecution[2] = input.substring(startIndex);

            execution = adjustedExecution;
        }

        return execution;
    }

    /**
     * Print the library into string.
     *
     * @return Result of printing the library.
     */
    private String printLibrary() {

        final StringBuilder builder = new StringBuilder();

        searchService.getLibrary()
                .stream()
                .forEach(r -> {
                    builder.append(r);
                    builder.append(LINE_SEPARATOR);
                });

        return builder.toString();
    }

    /**
     * Find item by given isbn.
     *
     * @param isbn Isbn to search the item in the library.
     * @return String representing the result of the search.
     */
    private String findByIsbn(final String isbn) {

        final Item item = searchService.findItemByIsbn(isbn);

        if (null == item)
            return NOT_FOUND;
        else
            return item.toString();
    }

    /**
     * Find items by given authors.
     *
     * @param authors Authors to search items in the library. Values are separated by coma: a,b,c.
     * @return String representing the list of items found on the search.
     */
    private String findByAuthors(final String authors) {

        final List<String> authorList = Stream.of(authors.split(",", -1))
                .collect(Collectors.toList());

        final List<Item> results = searchService.findItemsByAuthors(authorList);

        if (results.isEmpty())
            return NOT_FOUND;

        return printItems(results);
    }

    /**
     * Sort library by title.
     *
     * @return String representing the sorted library by title.
     */
    private String sortByTitle() {

        return printItems(
                searchService.sortByTitle());
    }

    /**
     * Add item to the library and generate new csv files.
     *
     * @param type Type of the item to add to the library. Example: book, magazine.
     * @param data Data of the item to add.
     * @return Result of the add operation.
     */
    private String addItem(final String type, final String data) {

        if ("book".equals(type)) {

            final Book book = LibraryService.getInstance()
                    .mapBookToItem
                    .apply(data);

            searchService.getLibrary()
                    .addBook(book);

            exportLibrary(type, searchService.getLibrary());

            return ADDED;
        }

        if ("magazine".equals(type)) {

            final Magazine magazine = LibraryService.getInstance()
                    .mapMagazineToItem
                    .apply(data);

            searchService.getLibrary()
                    .addMagazine(magazine);

            exportLibrary(type, searchService.getLibrary());

            return ADDED;
        }

        return BASE_ERROR;
    }

    /**
     * Export the given library into csv files.
     *
     * @param type    Type of item added to library.
     * @param library Library instance to export.
     */
    private void exportLibrary(final String type, final Library library) {

        try {

            csvWriter.generateCsv(library);

        } catch (IOException ioe) {

            LOG.error("Error generating csv files after adding {} - ", type, ioe);
        }
    }

    /**
     * Print the given results into a string.
     *
     * @param results Result to print into the string.
     * @return String with the results data.
     */
    private String printItems(final List<Item> results) {

        final StringBuilder builder = new StringBuilder();

        results.forEach(r -> {
            builder.append(r);
            builder.append(LINE_SEPARATOR);
        });

        return builder.toString();
    }

}

/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.service;

import lombok.Getter;
import org.echocat.kata.java.domain.Author;
import org.echocat.kata.java.domain.Item;
import org.echocat.kata.java.dto.Library;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class responsible of searching items inside library data.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class SearchService {

    /**
     * Define library instance used to make the searches.
     */
    @Getter
    private final Library library;

    /**
     * Constructor of the search service with given library.
     *
     * @param library Library to perform the search with the current service instance.
     */
    public SearchService(final Library library) {
        this.library = library;
    }

    /**
     * Find first item on the library with the given isbn.
     *
     * @param isbn Isbn to filter the search on the library.
     * @return Item instance found on the library. No record is found, null is returned.
     */
    Item findItemByIsbn(final String isbn) {

        return library.stream()
                .filter(t -> t.getIsbn()
                        .equals(isbn))
                .findFirst()
                .orElse(null);
    }

    /**
     * Find items by given authors.
     *
     * @param authors Author list to search on the library.
     * @return List of items found on the library.
     */
    List<Item> findItemsByAuthors(final List<String> authors) {

        return library.stream()
                .filter(t -> {

                    final List<Author> authorList = t.getAuthors();

                    for (Author author : authorList) {
                        if (authors.contains(author.getEmail()))
                            return true;
                    }

                    return false;

                }).collect(Collectors.toList());
    }

    /**
     * Sort the library items by title.
     *
     * @return List of items sorted by title.
     */
    List<Item> sortByTitle() {

        return library.stream()
                .sorted(
                        Comparator.comparing(Item::getTitle))
                .collect(Collectors.toList());
    }

}

/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.service;

import org.echocat.kata.java.domain.Author;
import org.echocat.kata.java.domain.Book;
import org.echocat.kata.java.domain.Magazine;
import org.echocat.kata.java.dto.Library;
import org.echocat.kata.java.utils.CsvReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Class responsible of reading library data into data transfer object.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class LibraryService {

    /**
     * CSV file separator, used to split lines of csv files.
     */
    private static final String SEPARATOR = ";";

    /**
     * Separator of list inside a csv column.
     */
    private static final String LIST_SEPARATOR = ",";

    /**
     * Define private instance to follow singleton pattern.
     */
    private static LibraryService instance;

    /**
     * Define authors cache to increase performance of mapping.
     */
    private Map<String, Author> authorCache = new HashMap<>();

    /**
     * Constructor of library service.
     */
    private LibraryService() {
    }

    /**
     * Retrieves instance of library service.
     *
     * @return Library service instance to load data into memory.
     */
    public static synchronized LibraryService getInstance() {

        if (null == instance)
            instance = new LibraryService();

        return instance;
    }

    /**
     * Read the library data using the given filenames for:
     *
     * <ul>
     *  <li> authors
     *  <li> books
     *  <li> magazines
     * </ul>
     *
     * @param authors   Authors filename. Example: authors.csv
     * @param books     Books filename. Example: books.csv
     * @param magazines Magazines filename. Example: magazines.csv
     * @return Library instance with the information of the given files.
     * @throws IOException Thrown when the files are not being able to be opened.
     */
    public Library readLibrary(final String authors, final String books, final String magazines) throws IOException {

        final List<Author> authorList = new CsvReader<Author>()
                .readData(authors, mapAuthorToItem);

        authorCache = authorList.stream()
                .collect(Collectors.toMap(Author::getEmail, Function.identity()));

        final List<Book> bookList = new CsvReader<Book>()
                .readData(books, mapBookToItem);

        final List<Magazine> magazineList = new CsvReader<Magazine>()
                .readData(magazines, mapMagazineToItem);

        final Library library = new Library();
        library.addBooks(bookList);
        library.addMagazines(magazineList);

        return library;
    }

    /**
     * Define function to map csv data into books.
     */
    Function<String, Book> mapBookToItem = line -> {

        final Book book = new Book();

        final String[] fields = line.split(SEPARATOR);

        book.setTitle(fields[0]);
        book.setIsbn(fields[1]);
        book.setDescription(fields[3]);

        final String[] authorField = fields[2].split(LIST_SEPARATOR);

        final List<Author> authors = new ArrayList<>();

        for (String email : authorField) {

            final Author author = authorCache.get(email);
            authors.add(author);
        }

        book.setAuthors(authors);

        return book;
    };

    /**
     * Define function to map csv data into magazines.
     */
    Function<String, Magazine> mapMagazineToItem = line -> {

        final Magazine magazine = new Magazine();

        final String[] fields = line.split(SEPARATOR);

        magazine.setTitle(fields[0]);
        magazine.setIsbn(fields[1]);
        magazine.setPublishedAt(fields[3]);

        final String[] authorField = fields[2].split(LIST_SEPARATOR);

        final List<Author> authors = new ArrayList<>();

        for (String email : authorField) {

            final Author author = authorCache.get(email);
            authors.add(author);
        }

        magazine.setAuthors(authors);

        return magazine;
    };

    /**
     * Define function to map csv data into authors.
     */
    private Function<String, Author> mapAuthorToItem = line -> {

        final Author author = new Author();

        final String[] fields = line.split(SEPARATOR);

        author.setEmail(fields[0]);
        author.setFirstName(fields[1]);
        author.setLastName(fields[2]);

        return author;
    };

}

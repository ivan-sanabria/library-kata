/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.domain;

import lombok.Data;

/**
 * Class to encapsulate author data stored in csv file.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Data
public class Author {

    /**
     * Define csv file separator.
     */
    private static final String SEPARATOR = ";";

    /**
     * Email of the author.
     */
    private String email;

    /**
     * First name of the author.
     */
    private String firstName;

    /**
     * Last name of the author.
     */
    private String lastName;

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Author {" +
                "email='" + getEmail() + '\'' +
                "firstName='" + getFirstName() + '\'' +
                "lastName='" + getLastName() + '\'' +
                '}';
    }

    /**
     * Generate the string is going to be the output shown on csv file.
     *
     * @return String representing the output shown on csv file.
     */
    public String toCsv() {
        return getEmail() + SEPARATOR +
                getFirstName() + SEPARATOR +
                getLastName();
    }

}

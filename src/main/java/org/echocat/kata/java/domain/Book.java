/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.stream.Collectors;

/**
 * Class to encapsulate book data stored in csv file.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Book extends Item {

    /**
     * Description of the book.
     */
    private String description;

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Book {" + "title='" + getTitle() + '\'' +
                "isbn='" + getIsbn() + '\'' +
                "description='" + getDescription() + '\'' +
                "authors='" + getAuthors() + '\'' +
                '}';
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toCsv() {
        return getTitle() + SEPARATOR +
                getIsbn() + SEPARATOR +
                getAuthors().stream()
                        .map(Author::getEmail)
                        .collect(Collectors.joining(",")) + SEPARATOR +
                getDescription();

    }

}

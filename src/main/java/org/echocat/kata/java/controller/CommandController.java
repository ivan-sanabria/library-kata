/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.controller;

import org.echocat.kata.java.service.CommandService;
import org.echocat.kata.java.service.LibraryService;
import org.echocat.kata.java.service.SearchService;
import org.echocat.kata.java.utils.CsvWriter;

import java.io.IOException;
import java.util.Scanner;

/**
 * Class to handle the main tasks specify on the kata.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class CommandController {

    /**
     * Define filename for author csv.
     */
    private static final String AUTHORS = "authors.csv";

    /**
     * Define filename for books csv.
     */
    private static final String BOOKS = "books.csv";

    /**
     * Define filename for magazines csv.
     */
    private static final String MAGAZINES = "magazines.csv";

    /**
     * Define default constructor of CommandController.
     */
    public CommandController() {}

    /**
     * Main function to execute on jar file.
     *
     * @param args Arguments given to the program.
     * @throws IOException Thrown when the files are not being able to read.
     */
    public static void main(String... args) throws IOException {

        final LibraryService libraryService = LibraryService.getInstance();

        final SearchService searchService = new SearchService(
                libraryService.readLibrary(AUTHORS, BOOKS, MAGAZINES));

        final CsvWriter csvWriter = new CsvWriter();
        final CommandService commandService = new CommandService(searchService, csvWriter);

        final Scanner scanner = new Scanner(System.in);

        while (scanner.hasNext()) {

            final String command = scanner.nextLine();
            final String result = commandService.execute(command);

            System.out.println(result);
        }
    }

}

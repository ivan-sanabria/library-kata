/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.dto;

import org.echocat.kata.java.domain.Book;
import org.echocat.kata.java.domain.Item;
import org.echocat.kata.java.domain.Magazine;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Class to encapsulate library data.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class Library {

    /**
     * Define default constructor of Library.
     */
    public Library() {}

    /**
     * Items on the library.
     */
    private List<Item> items = new ArrayList<>();

    /**
     * Returns true if this list contains no elements.
     *
     * @return True if this list contains no elements.
     */
    public boolean isEmpty() {
        return items.isEmpty();
    }

    /**
     * Add the given book to the items.
     *
     * @param book Book to add to the library.
     */
    public void addBook(final Book book) {
        items.add(book);
    }

    /**
     * Add the given book list to the items.
     *
     * @param books Book list to add to the library.
     */
    public void addBooks(final List<Book> books) {
        items.addAll(books);
    }

    /**
     * Add the given magazine to the items.
     *
     * @param magazine Magazine to add to the library.
     */
    public void addMagazine(final Magazine magazine) {
        items.add(magazine);
    }

    /**
     * Add the given magazine list to the items.
     *
     * @param magazines Magazine list to add to the library.
     */
    public void addMagazines(final List<Magazine> magazines) {
        items.addAll(magazines);
    }

    /**
     * Expose the stream to make search operations on library.
     *
     * @return A sequential stream over the elements in items.
     */
    public Stream<Item> stream() {
        return items.stream();
    }

}

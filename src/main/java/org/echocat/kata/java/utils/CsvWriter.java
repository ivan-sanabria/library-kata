/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.echocat.kata.java.domain.Author;
import org.echocat.kata.java.domain.Book;
import org.echocat.kata.java.domain.Item;
import org.echocat.kata.java.domain.Magazine;
import org.echocat.kata.java.dto.Library;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Class responsible of writing csv files.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class CsvWriter {

    /**
     * Define logger to track errors and show messages.
     */
    private static final Logger LOG = LogManager.getLogger(CsvWriter.class);

    /**
     * Define line separator for test cases.
     */
    private static final String LINE_SEPARATOR = System.lineSeparator();

    /**
     * Define extension of csv file.
     */
    private static final String EXTENSION = ".csv";

    /**
     * Define default constructor of CsvWriter.
     */
    public CsvWriter() {}

    /**
     * Generate the csv file using the given library.
     *
     * @param library Library instance is going to be writen in csv files.
     * @throws IOException Thrown when the file is not being able to be written.
     */
    public void generateCsv(final Library library) throws IOException {

        if (null != library && !library.isEmpty()) {

            printBooks(library);
            printMagazines(library);
            printAuthors(library);

        } else {

            LOG.warn("The given library to put into the csv files was empty. No files were generated.");
        }
    }

    /**
     * Generate books csv file with the given library.
     *
     * @param library Library to generate books.csv.
     * @throws IOException Thrown when the file is not being able to be written.
     */
    private void printBooks(final Library library) throws IOException {

        final String filename = "books" + EXTENSION;

        final List<Item> books = library.stream()
                .filter(Book.class::isInstance)
                .collect(Collectors.toList());

        final StringBuilder builder = new StringBuilder();

        builder.append("title;isbn;authors;description")
                .append(LINE_SEPARATOR);

        buildLibraryOutput(books, builder);
        printToCsv(filename, builder);
    }

    /**
     * Generate magazines csv file with the given library.
     *
     * @param library Library to generate magazines.csv.
     * @throws IOException Thrown when the file is not being able to be written.
     */
    private void printMagazines(final Library library) throws IOException {

        final String filename = "magazines" + EXTENSION;

        final List<Item> magazines = library.stream()
                .filter(Magazine.class::isInstance)
                .collect(Collectors.toList());

        final StringBuilder builder = new StringBuilder();

        builder.append("title;isbn;authors;publishedAt")
                .append(LINE_SEPARATOR);

        buildLibraryOutput(magazines, builder);
        printToCsv(filename, builder);
    }

    /**
     * Generate authors csv file with the given library.
     *
     * @param library Library to generate authors.csv.
     * @throws IOException Thrown when the file is not being able to be written.
     */
    private void printAuthors(final Library library) throws IOException {

        final String filename = "authors" + EXTENSION;

        final Set<Author> authors = library.stream()
                .flatMap(r ->
                        r.getAuthors()
                                .stream())
                .collect(Collectors.toSet());

        final StringBuilder builder = new StringBuilder();

        builder.append("email;firstname;lastname")
                .append(LINE_SEPARATOR);

        authors.forEach(a -> {
            builder.append(a.toCsv());
            builder.append(LINE_SEPARATOR);
        });

        printToCsv(filename, builder);
    }

    /**
     * Print the given output into a csv file with uui name.
     *
     * @param filename Filename of the file to store the given output.
     * @param output   Output is going to be printed into the csv file.
     * @throws IOException Thrown when the file is not being able to be written.
     */
    private void printToCsv(final String filename, final StringBuilder output) throws IOException {

        try (PrintWriter writer = new PrintWriter(filename, StandardCharsets.UTF_8)) {

            writer.append(
                    output.toString());
        }

        LOG.info("CSV {} generation file is done.", filename);
    }

    /**
     * Build the output is going to be included on the csv file using the given items.
     *
     * @param items   Items to print on the csv output.
     * @param builder Builder which contains the the csv output.
     */
    private void buildLibraryOutput(final List<Item> items, final StringBuilder builder) {

        items.forEach(r -> {
            builder.append(r.toCsv());
            builder.append(LINE_SEPARATOR);
        });
    }

}

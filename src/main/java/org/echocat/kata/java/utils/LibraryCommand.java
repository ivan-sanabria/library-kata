/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.utils;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * Enumeration to define supported commands for library.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public enum LibraryCommand {

    /**
     * Print the current items in the library.
     */
    PRINT("print", 0),

    /**
     * Find an item with the given isbn.
     */
    FIND_BY_ISBN("isbn", 1),

    /**
     * Find an item with the given list of authors.
     */
    FIND_BY_AUTHORS("authors", 1),

    /**
     * Sort the library by title.
     */
    SORT("sort", 0),

    /**
     * Add a new item to the library.
     */
    ADD("add", 2);

    /**
     * Command line supported by the application.
     */
    @Getter
    private String command;

    /**
     * Number of arguments supported by the command.
     */
    @Getter
    private int argsLength;

    /**
     * Private constructor to define the library commands.
     *
     * @param command    Command line.
     * @param argsLength Number of arguments that the command support.
     */
    LibraryCommand(final String command, final int argsLength) {
        this.command = command;
        this.argsLength = argsLength;
    }

    /**
     * Define command index for searching operations.
     */
    private static final Map<Integer, LibraryCommand> COMMAND_INDEX = new HashMap<>(LibraryCommand.values().length);

    /*
     * Fill the command index for searching operations.
     */
    static {

        for (LibraryCommand libraryCommand : LibraryCommand.values()) {

            final int argsLength = libraryCommand.getArgsLength();
            final String hashValue = libraryCommand.getCommand() + argsLength;

            COMMAND_INDEX.put(hashValue.hashCode(), libraryCommand);
        }
    }

    /**
     * Retrieves the Library Command instance by the given command and args length.
     *
     * @param command    Command to find inside the index.
     * @param argsLength Arguments length supported by the command to find inside the index.
     * @return Library Command found in the index, otherwise return null.
     */
    public static LibraryCommand findByCommandAndArgsLength(final String command, final int argsLength) {

        final String hashValue = command + argsLength;

        return COMMAND_INDEX.get(hashValue.hashCode());
    }

}

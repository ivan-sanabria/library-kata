/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Class responsible of reading csv files.
 *
 * @param <T> Type of item to read from CSV. Supported types: Book or Magazine.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class CsvReader<T> {

    /**
     * Define default constructor of CsvReader.
     */
    public CsvReader() {}

    /**
     * Read the data of the given filename of csv and mapped to specific domain using the given function.
     *
     * @param filename    Filename to read the data.
     * @param mapToDomain Lambda function to map the csv data to the T domain.
     * @return List of T containing the data provided on csv file.
     * @throws IOException Thrown when the file is not being able to be opened.
     */
    public List<T> readData(final String filename, final Function<String, T> mapToDomain) throws IOException {

        if (null == filename)
            return new ArrayList<>();

        final ClassLoader classLoader = Thread.currentThread()
                .getContextClassLoader();

        final InputStream inputStream = classLoader.getResourceAsStream(filename);

        if (null == inputStream)
            return new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {

            return bufferedReader.lines()
                    .skip(1)
                    .map(mapToDomain)
                    .collect(Collectors.toList());
        }
    }

}

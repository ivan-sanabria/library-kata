/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.controller;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Class to handle the test cases to handle given tasks.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
class CommandControllerTest {

    /**
     * Define line separator for test cases.
     */
    private static final String LINE_SEPARATOR = System.lineSeparator();

    /**
     * Define replacement for next line and tab characters.
     */
    private static final String REPLACEMENT = "";


    @Test
    @SuppressWarnings("AccessStaticViaInstance")
    void execute_tasks() throws IOException {

        final String[] args = {};
        final String input = "isbn 1024-5245-8584".concat(LINE_SEPARATOR);

        final Path expectedPath = Path.of("./results/execute-tasks.txt");
        final byte[] text = Files.readAllBytes(expectedPath);

        final String expected = new String(text)
                .replace(LINE_SEPARATOR, REPLACEMENT)
                .replaceAll("\\s{4,}", REPLACEMENT);

        final ByteArrayInputStream in = new ByteArrayInputStream(input.getBytes());
        final ByteArrayOutputStream controllerOut = new ByteArrayOutputStream();

        System.setIn(in);
        System.setOut(new PrintStream(controllerOut));

        final CommandController commandController = new CommandController();
        commandController.main(args);

        final String output = controllerOut.toString()
                .replace(LINE_SEPARATOR, REPLACEMENT);

        assertEquals(expected, output);
    }

}

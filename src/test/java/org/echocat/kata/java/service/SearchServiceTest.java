/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.service;

import org.echocat.kata.java.domain.Author;
import org.echocat.kata.java.domain.Book;
import org.echocat.kata.java.domain.Item;
import org.echocat.kata.java.domain.Magazine;
import org.echocat.kata.java.dto.Library;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class to handle the test cases to search items on library.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
class SearchServiceTest {

    /**
     * Define search service instance for testing purposes.
     */
    private static SearchService searchService;

    /**
     * Initialize testing instances.
     */
    @BeforeAll
    static void init() {

        final Author author = new Author();

        author.setEmail("null-walter@echocat.org");
        author.setFirstName("Paul");
        author.setLastName("Walter");

        final Author anotherAuthor = new Author();

        anotherAuthor.setEmail("null-lieblich@echocat.org");
        anotherAuthor.setFirstName("Werner");
        anotherAuthor.setLastName("Lieblich");

        final Magazine magazine = new Magazine();

        magazine.setTitle("Beautiful cooking");
        magazine.setIsbn("5454-5587-3210");
        magazine.setAuthors(Collections.singletonList(author));
        magazine.setPublishedAt("21.05.2011");

        final Book book = new Book();

        book.setTitle("Genial italienisch");
        book.setIsbn("1024-5245-8584");
        book.setAuthors(Arrays.asList(anotherAuthor, author));
        book.setDescription("Starkoch Jamie Oliver war mit seinem VW-Bus in Italien unterwegs...");

        final Magazine anotherMagazine = new Magazine();

        anotherMagazine.setTitle("The Wine Connoisseur");
        anotherMagazine.setIsbn("2547-8548-2541");
        anotherMagazine.setAuthors(Collections.singletonList(author));
        anotherMagazine.setPublishedAt("12.12.2011");

        final Library library = new Library();

        library.addBook(book);
        library.addMagazines(Arrays.asList(magazine, anotherMagazine));

        searchService = new SearchService(library);
    }


    @Test
    void given_library_not_found_item_by_isbn() {

        final String isbn = "12345";

        final Item item = searchService.findItemByIsbn(isbn);
        assertNull(item);
    }

    @Test
    void given_library_found_item_by_isbn() {

        final String isbn = "5454-5587-3210";

        final Author author = new Author();

        author.setEmail("null-walter@echocat.org");
        author.setFirstName("Paul");
        author.setLastName("Walter");

        final Magazine expectedItem = new Magazine();

        expectedItem.setTitle("Beautiful cooking");
        expectedItem.setIsbn(isbn);
        expectedItem.setAuthors(Collections.singletonList(author));
        expectedItem.setPublishedAt("21.05.2011");

        final Item item = searchService.findItemByIsbn(isbn);
        assertEquals(item, expectedItem);
    }

    @Test
    void given_library_not_found_item_by_authors() {

        final List<String> authors = Collections.singletonList("null-gustafsson@echocat.org");

        final List<Item> items = searchService.findItemsByAuthors(authors);
        assertTrue(items.isEmpty());
    }

    @Test
    void given_library_found_items_by_authors() {

        final List<String> authors = Arrays.asList("null-gustafsson@echocat.org", "null-walter@echocat.org");

        final List<Item> items = searchService.findItemsByAuthors(authors);
        assertEquals(3, items.size());
    }

    @Test
    void given_library_sort_by_title() {

        final List<Item> items = searchService.sortByTitle();

        assertEquals(3, items.size());
        assertEquals("Beautiful cooking", items.get(0).getTitle());
        assertEquals("Genial italienisch", items.get(1).getTitle());
        assertEquals("The Wine Connoisseur", items.get(2).getTitle());
    }

}

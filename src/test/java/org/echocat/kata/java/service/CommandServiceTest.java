/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.service;

import org.echocat.kata.java.domain.Author;
import org.echocat.kata.java.domain.Book;
import org.echocat.kata.java.domain.Magazine;
import org.echocat.kata.java.dto.Library;
import org.echocat.kata.java.utils.CsvValidator;
import org.echocat.kata.java.utils.CsvWriter;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Class to handle the test cases to handle command inputs.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
class CommandServiceTest {

    /**
     * Define line separator for test cases.
     */
    private static final String LINE_SEPARATOR = System.lineSeparator();

    /**
     * Define replacement for next line and tab characters.
     */
    private static final String REPLACEMENT = "";


    @Test
    void execute_null_command() {

        final SearchService searchService = mock(SearchService.class);
        final CsvWriter csvWriter = mock(CsvWriter.class);

        final CommandService commandService = new CommandService(searchService, csvWriter);

        final String expected = "Command not supported";
        final String result = commandService.execute(null);

        assertEquals(expected, result);
    }

    @Test
    void execute_empty_command() {

        final SearchService searchService = mock(SearchService.class);
        final CsvWriter csvWriter = mock(CsvWriter.class);

        final CommandService commandService = new CommandService(searchService, csvWriter);

        final String expected = "Command not supported";
        final String result = commandService.execute("");

        assertEquals(expected, result);
    }

    @Test
    void execute_invalid_commands() {

        final SearchService searchService = mock(SearchService.class);
        final CsvWriter csvWriter = mock(CsvWriter.class);

        final CommandService commandService = new CommandService(searchService, csvWriter);

        final String expected = "Command not supported";

        String result = commandService.execute("Hello World Command");
        assertEquals(expected, result);

        result = commandService.execute("isbn a b");
        assertEquals(expected, result);

        result = commandService.execute("authors a b");
        assertEquals(expected, result);

        result = commandService.execute("add a b c");
        assertEquals(expected, result);

        result = commandService.execute("add newspaper c");
        assertEquals(expected, result);
    }

    @Test
    void execute_add_with_writing_failure() throws IOException {

        final SearchService searchService = mock(SearchService.class);
        final CsvWriter csvWriter = mock(CsvWriter.class);

        final Library library = new Library();

        when(searchService.getLibrary())
                .thenReturn(library);

        final IOException exception = new AccessDeniedException("/incorrect-location&");

        doThrow(exception).when(csvWriter)
                .generateCsv(any(Library.class));

        final CommandService commandService = new CommandService(searchService, csvWriter);

        final String expected = "ADDED";
        final String result = commandService.execute("add magazine Records of Tomorrow;4545-8541-2020;null-ferdinand@echocat.org;10.07.2020");

        assertEquals(expected, result);
    }

    @Test
    void execute_multiple_commands_with_2_invalid_commands() throws IOException {

        final SearchService searchService = mock(SearchService.class);
        final CsvWriter csvWriter = mock(CsvWriter.class);

        final Author author = new Author();

        author.setEmail("null-ferdinand@echocat.org");
        author.setFirstName("Franz");
        author.setLastName("Ferdinand");

        final Magazine magazine = new Magazine();

        magazine.setTitle("Gourmet");
        magazine.setIsbn("2365-8745-7854");
        magazine.setAuthors(Collections.singletonList(author));
        magazine.setPublishedAt("14.06.2010");

        final Book book = new Book();

        book.setTitle("Das Perfekte Dinner. Die besten Rezepte");
        book.setIsbn("2221-5548-8585");
        book.setAuthors(Collections.singletonList(author));
        book.setDescription("Sie wollen auch ein perfektes Dinner kreieren? Mit diesem Buch gelingt es Ihnen!");

        final Library library = new Library();

        library.addMagazine(magazine);
        library.addBook(book);

        when(searchService.findItemByIsbn(magazine.getIsbn()))
                .thenReturn(magazine);

        when(searchService.findItemsByAuthors(Arrays.asList("null-mueller@echocat.org", "null-ferdinand@echocat.org")))
                .thenReturn(Collections.singletonList(magazine));

        when(searchService.findItemByIsbn(book.getIsbn()))
                .thenReturn(book);

        final CommandService commandService = new CommandService(searchService, csvWriter);

        String expected = "Command not supported";
        String result = commandService.execute("isbn 2365-8745-7854 1024-5245-8584");

        assertEquals(expected, result);

        Path expectedPath = Path.of("./results/execute-commands-magazine-result.txt");
        byte[] text = Files.readAllBytes(expectedPath);

        expected = new String(text)
                .replace(LINE_SEPARATOR, REPLACEMENT)
                .replaceAll("\\s{4,}", REPLACEMENT);

        result = commandService.execute("isbn 2365-8745-7854");
        assertEquals(expected, result);

        result = commandService.execute("authors null-mueller@echocat.org,null-ferdinand@echocat.org")
                .replace(LINE_SEPARATOR, REPLACEMENT);

        assertEquals(expected, result);

        expectedPath = Path.of("./results/execute-commands-book-result.txt");
        text = Files.readAllBytes(expectedPath);

        expected = new String(text)
                .replace(LINE_SEPARATOR, REPLACEMENT)
                .replaceAll("\\s{4,}", REPLACEMENT);

        result = commandService.execute("isbn 2221-5548-8585");
        assertEquals(expected, result);
    }

    @Test
    void execute_multiple_commands_with_0_invalid_commands() throws IOException {

        final LibraryService libraryService = LibraryService.getInstance();

        final SearchService searchService = new SearchService(
                libraryService.readLibrary("authors.csv", "books.csv", "magazines.csv"));

        final CsvWriter csvWriter = new CsvWriter();
        final CommandService commandService = new CommandService(searchService, csvWriter);

        int expectedLength = 5617;
        String originalResult = commandService.execute("print");

        assertEquals(expectedLength, originalResult.length());

        String result = commandService.execute("sort");
        assertEquals(expectedLength, result.length());

        assertNotEquals(result, originalResult);

        String expected = "NOT FOUND";
        result = commandService.execute("isbn 12345");

        assertEquals(expected, result);

        expected = "NOT FOUND";
        result = commandService.execute("authors null-sanabria@echocat.org");

        assertEquals(expected, result);

        expected = "ADDED";
        result = commandService.execute("add magazine Records of Tomorrow;4545-8541-2020;null-ferdinand@echocat.org;10.07.2020");

        assertEquals(expected, result);
        CsvValidator.validateCsvFiles();

        expected = "ADDED";
        result = commandService.execute("add book Crime and Punishment;2221-5548-9090;null-ferdinand@echocat.org;This is a book for testing");

        assertEquals(expected, result);
        CsvValidator.validateCsvFiles();
    }

}

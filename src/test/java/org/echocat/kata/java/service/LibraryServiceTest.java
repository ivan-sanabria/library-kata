/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.service;

import org.echocat.kata.java.domain.Author;
import org.echocat.kata.java.domain.Book;
import org.echocat.kata.java.domain.Magazine;
import org.echocat.kata.java.dto.Library;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class to handle the test cases to read library data.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
class LibraryServiceTest {


    @Test
    void given_wrong_filenames_return_empty_library() throws IOException {

        final Library library = LibraryService.getInstance()
                .readLibrary(null, null, null);

        assertTrue(library.isEmpty());
    }

    @Test
    void given_empty_filenames_return_empty_library() throws IOException {

        final Library library = LibraryService.getInstance()
                .readLibrary("", "", "");

        assertTrue(library.isEmpty());
    }

    @Test
    void given_incomplete_filenames_return_full_library() throws IOException {

        final Author author = new Author();

        author.setEmail("null-walter@echocat.org");
        author.setFirstName("Paul");
        author.setLastName("Walter");

        final Author anotherAuthor = new Author();

        anotherAuthor.setEmail("null-lieblich@echocat.org");
        anotherAuthor.setFirstName("Werner");
        anotherAuthor.setLastName("Lieblich");

        final Magazine magazine = new Magazine();

        magazine.setTitle("Beautiful cooking");
        magazine.setIsbn("5454-5587-3210");
        magazine.setAuthors(Collections.singletonList(author));
        magazine.setPublishedAt("21.05.2011");

        final Book book = new Book();

        book.setTitle("Das Perfekte Dinner. Die besten Rezepte");
        book.setIsbn("2221-5548-8585");
        book.setAuthors(Collections.singletonList(anotherAuthor));
        book.setDescription("Sie wollen auch ein perfektes Dinner kreieren? Mit diesem Buch gelingt es Ihnen!");

        final Library library = LibraryService.getInstance()
                .readLibrary("authors.csv", null, "magazines.csv");

        assertFalse(library.isEmpty());

        assertTrue(library.stream()
                .anyMatch(item -> item.equals(magazine)));

        assertFalse(library.stream()
                .anyMatch(item -> item.equals(book)));
    }

    @Test
    void given_proper_filenames_return_full_library() throws IOException {

        final Author author = new Author();

        author.setEmail("null-walter@echocat.org");
        author.setFirstName("Paul");
        author.setLastName("Walter");

        final Author anotherAuthor = new Author();

        anotherAuthor.setEmail("null-lieblich@echocat.org");
        anotherAuthor.setFirstName("Werner");
        anotherAuthor.setLastName("Lieblich");

        final Magazine magazine = new Magazine();

        magazine.setTitle("Beautiful cooking");
        magazine.setIsbn("5454-5587-3210");
        magazine.setAuthors(Collections.singletonList(author));
        magazine.setPublishedAt("21.05.2011");

        final Book book = new Book();

        book.setTitle("Das Perfekte Dinner. Die besten Rezepte");
        book.setIsbn("2221-5548-8585");
        book.setAuthors(Collections.singletonList(anotherAuthor));
        book.setDescription("Sie wollen auch ein perfektes Dinner kreieren? Mit diesem Buch gelingt es Ihnen!");

        final Library library = LibraryService.getInstance()
                .readLibrary("authors.csv", "books.csv", "magazines.csv");

        assertFalse(library.isEmpty());

        assertTrue(library.stream()
                .anyMatch(item -> item.equals(magazine)));

        assertTrue(library.stream()
                .anyMatch(item -> item.equals(book)));
    }

}

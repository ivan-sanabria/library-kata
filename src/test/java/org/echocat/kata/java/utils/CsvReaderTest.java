/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.utils;

import org.echocat.kata.java.domain.Author;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * Class to handle the test cases to read csv file.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
class CsvReaderTest {

    /**
     * Define instance for testing purposes.
     */
    private final CsvReader<Author> reader = new CsvReader<>();


    @Test
    void given_null_return_empty_list() throws IOException {

        final List<Author> authorList = reader.readData(null, mapAuthorToItem);
        assertTrue(authorList.isEmpty());
    }

    @Test
    void given_filename_return_empty_list() throws IOException {

        final List<Author> authorList = reader.readData("library.csv", mapAuthorToItem);
        assertTrue(authorList.isEmpty());
    }

    @Test
    void given_filename_return_list_authors() throws IOException {

        final List<Author> authorList = reader.readData("authors.csv", mapAuthorToItem);
        assertFalse(authorList.isEmpty());
    }

    /**
     * Define function to map csv data into authors.
     */
    private Function<String, Author> mapAuthorToItem = (line) -> {

        final Author author = new Author();

        final String[] fields = line.split(";");

        author.setEmail(fields[0]);
        author.setFirstName(fields[1]);
        author.setLastName(fields[2]);

        return author;
    };

}

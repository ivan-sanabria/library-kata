/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.utils;

import org.echocat.kata.java.domain.Author;
import org.echocat.kata.java.domain.Book;
import org.echocat.kata.java.domain.Magazine;
import org.echocat.kata.java.dto.Library;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class to handle the test cases for writing content to csv file.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
class CsvWriterTest {

    /**
     * Define instance for testing purposes.
     */
    private final CsvWriter csvWriter = new CsvWriter();


    @Test
    void no_file_generation_with_null_result() throws IOException {

        csvWriter.generateCsv(null);
        assertTrue(CsvValidator.filesNotFound());
    }

    @Test
    void no_file_generation_with_empty_result() throws IOException {

        final Library library = new Library();

        csvWriter.generateCsv(library);
        assertTrue(CsvValidator.filesNotFound());
    }

    @Test
    void file_generation_with_full_library() throws IOException {

        final Author author = new Author();

        author.setEmail("null-walter@echocat.org");
        author.setFirstName("Paul");
        author.setLastName("Walter");

        final Author anotherAuthor = new Author();

        anotherAuthor.setEmail("null-lieblich@echocat.org");
        anotherAuthor.setFirstName("Werner");
        anotherAuthor.setLastName("Lieblich");

        final Magazine magazine = new Magazine();

        magazine.setTitle("Beautiful cooking");
        magazine.setIsbn("5454-5587-3210");
        magazine.setAuthors(Collections.singletonList(author));
        magazine.setPublishedAt("21.05.2011");

        final Book book = new Book();

        book.setTitle("Das Perfekte Dinner. Die besten Rezepte");
        book.setIsbn("2221-5548-8585");
        book.setAuthors(Collections.singletonList(anotherAuthor));
        book.setDescription("Sie wollen auch ein perfektes Dinner kreieren? Mit diesem Buch gelingt es Ihnen!");

        final Library library = new Library();

        library.addMagazine(magazine);
        library.addBook(book);

        csvWriter.generateCsv(library);
        CsvValidator.validateCsvFiles();
    }

}

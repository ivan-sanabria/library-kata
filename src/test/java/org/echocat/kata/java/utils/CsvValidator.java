/*
 * Copyright 2020 echocat, Iván Camilo Sanabria
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON INFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package org.echocat.kata.java.utils;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class to handle the validation and deletion of csv file generated on test cases.
 *
 * @author Iván Camilo Sanabria (icsanabriar@googlemail.com)
 * @since  1.0.0
 */
public class CsvValidator {

    /**
     * Define local directory location.
     */
    private static final String LOCAL_DIRECTORY = ".";

    /**
     * Define extension of files to scan.
     */
    private static final String EXTENSION = ".csv";

    /**
     * Validate the application generates the a csv files which contains the given library data.
     */
    public static void validateCsvFiles() {

        final File dir = new File(LOCAL_DIRECTORY);

        final String[] filenames = dir.list();
        assertNotNull(filenames);

        final String authors = "authors.csv";
        validateFileExist(filenames, authors);

        final String books = "books.csv";
        validateFileExist(filenames, books);

        final String magazines = "magazines.csv";
        validateFileExist(filenames, magazines);
    }

    /**
     * Validate that CSV file is not found in the current directory.
     *
     * @return A boolean representing if csv files are found.
     */
    static boolean filesNotFound() {

        final File dir = new File(LOCAL_DIRECTORY);

        final String[] filenames = dir.list();
        assertNotNull(filenames);

        for (String filename : filenames) {

            if (filename.endsWith(EXTENSION))
                return false;
        }

        return true;
    }

    /**
     * Validate that the give target file exists.
     *
     * @param filenames  List of csv filenames found in the root directory.
     * @param targetFile Target filename to validate.
     */
    private static void validateFileExist(final String[] filenames, final String targetFile) {

        File currentFile = null;

        for (String filename : filenames) {

            if (targetFile.equals(filename)) {
                currentFile = new File(filename);
                assertTrue(currentFile.delete());
            }
        }

        assertNotNull(currentFile);
        assertFalse(currentFile.exists());
    }

}

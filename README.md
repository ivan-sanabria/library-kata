# Java Kata - Part 1: Library

version 1.4.0 - 21/02/2023

[![License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://opensource.org/licenses/MIT)
[![Build](https://img.shields.io/bitbucket/pipelines/ivan-sanabria/library-kata.svg)](http://bitbucket.org/ivan-sanabria/library-kata/addon/pipelines/home#!/results/branch/master/page/1)
[![PR](https://img.shields.io/bitbucket/pr/ivan-sanabria/library-kata.svg)](http://bitbucket.org/ivan-sanabria/library-kata/pull-requests)
[![Quality Gate](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_library-kata&metric=alert_status)](https://sonarcloud.io/project/overview?id=Iván-Camilo-Sanabria-Rincón_library-kata)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_library-kata&metric=bugs)](https://sonarcloud.io/project/issues?resolved=false&types=BUG&id=Iván-Camilo-Sanabria-Rincón_library-kata)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_library-kata&metric=coverage)](https://sonarcloud.io/component_measures?id=Iván-Camilo-Sanabria-Rincón_library-kata&metric=coverage)
[![Size](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_library-kata&metric=ncloc)](https://sonarcloud.io/code?id=Iván-Camilo-Sanabria-Rincón_library-kata)
[![TechnicalDebt](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_library-kata&metric=sqale_index)](https://sonarcloud.io/component_measures?metric=Maintainability&id=Iván-Camilo-Sanabria-Rincón_library-kata)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=Iván-Camilo-Sanabria-Rincón_library-kata&metric=vulnerabilities)](https://sonarcloud.io/project/issues?resolved=false&types=VULNERABILITY&id=Iván-Camilo-Sanabria-Rincón_library-kata)

## Specification

The specification of the kata could be in the [README.md](https://github.com/echocat/java-kata-1).

## Requirements

- JDK 14.x
- Maven 3.6.3
- IDE for JAVA (Eclipse, Netbeans, IntelliJ).

## Running Application inside IDE

To run the application on your IDE:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Integrate your project with IDE:
    - [Eclipse](http://books.sonatype.com/m2eclipse-book/reference/creating-sect-importing-projects.html)
    - [Netbeans](http://wiki.netbeans.org/MavenBestPractices)
    - [IntelliJ]( https://www.jetbrains.com/idea/help/importing-project-from-maven-model.html)
5. Open IDE and run the class **CommandController.java**.

## Running Application on Terminal

To run the application on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean compile assembly:single
    java -jar target/library-kata-1.4.0.jar
```

## Running Unit Tests on Terminal

To run the unit tests on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean test
```

## Check Application Test Coverage using Jacoco

To verify test coverage on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn clean verify
    open target/site/jacoco/index.html
```

## List Dependency Licenses

To generate list of dependency licenses on terminal:

1. Verify the version of your JDK - 14.x or higher.
2. Verify the version of Maven - 3.6.3 or higher.
3. Download the source code from repository.
4. Open a terminal.
5. Go to the root location of the source code.
6. Execute the commands:

```bash
    mvn project-info-reports:dependencies
    open target/site/dependencies.html
```

## Example of Supported Commands

These are the supported commands of the application:

```bash
    # Print the items of the library
    print
    # Search an item by isbn
    isbn 1024-5245-8584
    # Search an item by authors
    authors null-lieblich@echocat.org,null-walter@echocat.org
    # Sort the library by title
    sort
    # Add a book into the library
    add book Remembrance of Things Past;7777-7777-7777;null-ferdinand@echocat.org,null-lieblich@echocat.org;Longest book ever...
    # Add a magazine into the library
    add magazine The Recruiter;2145-8548-3325;null-mueller@echocat.org;Boring magazine ever...
```

# Contact Information

Email: icsanabriar@googlemail.com